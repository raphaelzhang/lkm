#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sysrq.h>
#include <linux/kmod.h>

static void call_monitor(int key)
{
//	static char* argv[] = {"/usr/bin/gedit", 0};
//	static char* argv[] = {"/usr/bin/wall", "-n", "hello", 0};
//	static char* argv[] = {"/usr/bin/strace", "/usr/bin/gedit", 0};
	static char* argv[] = {"/home/raphael/temp/qthello/bin/hello", 0};
	static char* envp[] = {
		"DISPLAY=:0",
		"XAUTHORITY=/home/raphael/.Xauthority",
/*
		"HOME=/",
		"PATH=/sbin:/bin:/usr/sbin:/usr/bin",
		"TERM=linux",
		"DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus",
		"XDG_RUNTIME_DIR=/run/user/1000",
		"XDG_SESSION_PATH=/org/freedesktop/DisplayManager/Session0",
		"DESKTOP_SESSION=deepin",
		"XMODIFIERS=@im=fcitx",
*/
		0};

	call_usermodehelper(argv[0], argv, envp, UMH_NO_WAIT);
}

static struct sysrq_key_op call_monitor_op = {
	.handler = call_monitor,
	.help_msg = "invoke system monitor(a)",
	.action_msg = "Invoke System Monitor",
	.enable_mask = SYSRQ_ENABLE_KEYBOARD,
};

int init_module()
{
	return register_sysrq_key('a', &call_monitor_op);
}

void cleanup_module()
{
	unregister_sysrq_key('a', &call_monitor_op);
}

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Deepin");
MODULE_DESCRIPTION("Monitor Hook");
