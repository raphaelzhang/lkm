#ifndef _DGST_H__
#define _DGST_H__

#define DGST_SIZE   8
#define PROCFS_NAME "dgst"

#define DGST_IOCTL_MAGIC 0x81
#define DGST_IOCTL_READ  _IOR(DGST_IOCTL_MAGIC, 0, long)
#define DGST_IOCTL_RESET _IO(DGST_IOCTL_MAGIC, 1)

#endif
