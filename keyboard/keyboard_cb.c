#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/keyboard.h>
#include <linux/semaphore.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>

#define SIZE			(1<<16)
#define PROCFS_NAME		"kb_changes"

static struct semaphore sem_changes;

static int buffer[SIZE];
static int buf_pos = 0;

static int kb_notifier_cb(struct notifier_block *blk, unsigned long code, void *_param)
{
	struct keyboard_notifier_param *param = _param;
	int ret = NOTIFY_OK;

	if (!param->down)
		return ret;

	switch (code) {
	case KBD_KEYCODE:
		pr_info("kb-cb: %d, shift: %d, value: %d\n", buf_pos, param->shift, param->value);
		if (down_interruptible(&sem_changes) != 0)
			return -EINTR;
		if (buf_pos+2 < SIZE) {
			buffer[buf_pos++] = param->shift;
			buffer[buf_pos++] = param->value;
		}
		up(&sem_changes);
		break;
	default:
		break;
	}
	return ret;
}

static struct notifier_block kbnb = {
	.notifier_call = kb_notifier_cb,
};

#define MIN_UNIT	(sizeof(int)*2)

static ssize_t read_kb_changes(struct file* filp, char __user* buf, size_t size, loff_t* offset)
{
	if (size < MIN_UNIT)
		return -EINVAL;

	int data_size = 0;
	if (down_interruptible(&sem_changes) != 0)
		return -EINTR;

	int data_left = buf_pos*sizeof(int);
	data_size = size < data_left ? size : data_left;
	data_size = (data_size / MIN_UNIT) * MIN_UNIT;
	if (data_size > 0) {
		if(copy_to_user(buf, buffer, data_size) == 0) {
			int ints = data_size / sizeof(int);
			if (ints < buf_pos)
				memmove(buffer, buffer+ints, buf_pos*sizeof(int) - data_size);
			buf_pos -= ints;
		} else {
			data_size = 0;
		}
	}
			
	up(&sem_changes);
	return data_size;
}

static struct file_operations procfs_ops = {
	.owner = THIS_MODULE,
	.read = read_kb_changes,
	.llseek = no_llseek,
};

int __init init_module()
{
	sema_init(&sem_changes, 1);
	struct proc_dir_entry* procfs_entry = proc_create(PROCFS_NAME, 0666, 0, &procfs_ops);
	if (procfs_entry == 0) {
		pr_warn("%s already exists?\n", PROCFS_NAME);
		return -1;
	}

	int ret = register_keyboard_notifier(&kbnb);
	if (ret != 0) {
		pr_err("reg_kb_notifier failed: %d\n", ret);
		return ret;
	}

	pr_info("reg_kb_notifier done\n");
	return 0;
}

void __exit cleanup_module()
{
	unregister_keyboard_notifier(&kbnb);
	remove_proc_entry(PROCFS_NAME, 0);
	pr_info("unreg_kb_notifier done\n");
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("deepin");
MODULE_DESCRIPTION("Keyboard Change Detection Module");
