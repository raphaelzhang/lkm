#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <net/netlink.h>
#include <net/net_namespace.h>
#include <linux/kthread.h>
#include <linux/delay.h>

#define MY_NLS	22

static struct task_struct* runner = 0;

static void send_data(struct sock* s)
{
	char *msg = "hello, kernel";
	struct sk_buff* skb = nlmsg_new(NLMSG_ALIGN(strlen(msg)+2), GFP_KERNEL);
	if (!skb) {
		pr_err("skb allocate failed\n");
		return;
	}

	struct nlmsghdr *nlh = nlmsg_put(skb, 0, 1, NLMSG_DONE, strlen(msg)+2, 0);
	if (nlh == 0) {
		pr_err("n_p failed\n");
		nlmsg_free(skb);
		return;
	}
	strcpy(nlmsg_data(nlh), msg);
	pr_info("sending data...\n");
	nlmsg_end(skb, nlh);

	int r = nlmsg_multicast(s, skb, 0, MY_NLS, GFP_KERNEL);
	if (r)
		pr_err("n_m failed\n");
	else
		pr_info("n_m ok\n");
}

static int run(void* param)
{
	// struct sock* s = netlink_kernel_create(&init_net, NETLINK_USERSOCK, 0);
	struct sock* s = (struct sock *)param;

	if (!s) {
		pr_err("n_k_c failed\n");
	} else {
		pr_info("n_k_c ok\n");
		send_data(s);
		netlink_kernel_release(s);
	}

	while (1) {
		if (kthread_should_stop()) {
			pr_info("runner exiting...\n");
			break;
		}

		msleep(100);
	}
	return 0;
}

int __init init_module()
{
	struct sock* s = netlink_kernel_create(&init_net, NETLINK_USERSOCK, 0);
	runner = kthread_run(run, s, "nls_runner");
	pr_info("nls_test loaded\n");
	return 0;
}

void __exit cleanup_module()
{
	if (runner) {
		kthread_stop(runner);
	}

	pr_info("nls_test unloaded\n");
}
