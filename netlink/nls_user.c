#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#define MY_NLS	22

int main()
{
	int s = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_USERSOCK);
	if (s < 0) {
		perror("socket err");
		return 1;
	}

	struct sockaddr_nl l_local;
	memset(&l_local, 0, sizeof(l_local));
	l_local.nl_family = AF_NETLINK;
	// l_local.nl_groups = MY_NLS;
	l_local.nl_pid = getpid();

	if (bind(s, (struct sockaddr *)&l_local, sizeof(struct sockaddr_nl)) == -1) {
		printf("bind err\n");
		close(s);
		return 2;
	}

	int grp = MY_NLS;
	setsockopt(s, 270, NETLINK_ADD_MEMBERSHIP, &grp, sizeof(grp));

	char buf[1024] = {};
	printf("before-recv\n");
	int len = recv(s, buf, sizeof(buf), 0);
	printf("recv: %d\n", len);
	if (len > 0) {
		struct nlmsghdr * nlh = (struct nlmsghdr *)buf;
		if (nlh->nlmsg_type == NLMSG_DONE) {
			char *msg = (char *)NLMSG_DATA(nlh);
			printf("msg: %s\n", msg);
		} else {
			printf("msg-type: %d\n", nlh->nlmsg_type);
		}
	} else {
		perror("recv");
	}
	
	close(s);
	printf("done\n");
}
