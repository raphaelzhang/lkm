#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/lsm_hooks.h>
#include <linux/sched.h>
#include <linux/kallsyms.h>

#include <linux/socket.h>

static int block_sogou_inet_socket(int family, int type, int protocol, int kern)
{
    if (family != AF_INET || kern == 1)
        return 0;

    struct task_struct* ts = current;
    while (ts != 0 && ts->tgid > 1) {
        struct task_struct* leader = ts->group_leader;
        if (!leader)
            break;

        if (strcmp(leader->comm, "sogou-qimpanel") == 0)
            return -EPERM;
        ts = ts->parent;
    }

    return 0;
}

static struct security_hook_list triage_hooks[] = {
    //LSM_HOOK_INIT(socket_create, block_sogou_inet_socket),
    {.hook = {.socket_create = block_sogou_inet_socket}},
};

int init_module()
{
    uintptr_t p = kallsyms_lookup_name("security_hook_heads");
    struct security_hook_heads* sec_hook_heads = (struct security_hook_heads*)p;

#ifdef DEBUG
    print_hex_dump(KERN_INFO, "SEC-HOOK-HEADS ", DUMP_PREFIX_ADDRESS, 16, 1, (void *)p, 128, true);
    pr_info("triage-hooks size: %ld\n", ARRAY_SIZE(triage_hooks));
    // print_hex_dump(KERN_INFO, "OUR-HOOK-HEADS ", DUMP_PREFIX_ADDRESS, 16, 1, sec_hook_heads, 128, true);

    struct list_head lh = sec_hook_heads->socket_create;
    pr_info("list-head: %p, %p, %p, %p\n", lh.next, lh.prev, lh.next->prev, lh.prev->next);
#endif

    // INIT_LIST_HEAD(&triage_hooks[0].list);
    triage_hooks[0].head = &sec_hook_heads->socket_create;
    security_add_hooks(triage_hooks, ARRAY_SIZE(triage_hooks));
    return 0;
}

void cleanup_module()
{
#ifdef CONFIG_SECURITY_SELINUX_DISABLE
    security_delete_hooks(triage_hooks, ARRAY_SIZE(triage_hooks));
#else
    for (int i = 0; i < ARRAY_SIZE(triage_hooks); i++)
        list_del_rcu(&triage_hooks[i].list);
#endif
}

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Raphael");
MODULE_DESCRIPTION("LSM Hook Demo");
