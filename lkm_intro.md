# Linux Kernel Module Development Introduction

## hello, world

Linux内核模块是运行在Linux内核空间的模块，它可以执行高权限操作，包括访问甚至修改内核里面的数据，也可以随着内核一起运行。但是内核模块的开发对我们来说还是一件陌生的事，这几天刚好项目需要，正好学习了一下，把心得写下来与大家分享。

学习任何开发的时候，第一件事就是要写一个hello world的程序，这几乎是约定俗成的事了，废话少说，放码过来。我们先把代码列举出来，参见[hello world源码](https://bitbucket.org/raphaelzhang/lkm/raw/cc99c6/hello/hello.c)与[Makefile源码](https://bitbucket.org/raphaelzhang/lkm/raw/cc99c6/hello/Makefile)。

本内核模块编译和测试的步骤如下：

1. 安装gcc、make和linux内核头文件，即`sudo apt-get install gcc make linux-headers-$(uname -r)`
2. 创建一个目录，并将源代码文件与Makefile存放在此目录下
3. 进入此目录编译代码，即运行`make`
4. 按`Ctrl-Alt-F2`切换到tty2下，并登录系统，进入此目录
5. 运行`sudo insmod hello.ko`挂载此内核模块，你将看到hello world，在前面还会有被中括号包围的时间戳
6. 运行`lsmod | grep --color hello`可以看到hello内核模块已经被载入
7. 运行`sudo rmmod hello`卸载此内核模块，你将看到bye~ world，在前面还会有被中括号包围的时间戳

恭喜你，你的内核模块的第一个程序已经顺利运行了(此处应有掌声;)！

下面我们来一一解释下代码里各行的意义，免得大家一头雾水，知其然不知其所以然。

首先是hello.c的源代码。在源代码里，首先需要注意的就是我们没有include任何普通C程序的头文件，例如stdio.h、string.h等。这是因为在内核态编程，我们不能使用glibc的任何函数，因为显然glibc的很多函数是依赖于Linux的系统调用的，而系统调用又是给用户态的程序提供的内核的接口，在内核里调用用户态的代码显然不靠谱。实际上，在内核模块开发过程中，我们include的基本上都是linux/开头的各头文件。

其次我们需要注意的是此代码没有main函数，只有`init_module`与`cleanup_module`函数，从字面上，也从运行效果上来看，前者显然是在本内核模块被载入(即root运行`insmod hello.ko`)时被调用的，后者则应该是本内核模块被卸载(即root运行`rmmod hello`)时被调用的。在此处，你实际上也可以定义自己的初始化与卸载函数名，只要满足相应的函数签名要求，然后通过`module_init`与`module_exit`调用设定初始化与卸载函数即可。如下所示：

```C
int init_hello(void)
{
    printk(KERN_ALERT "hello, world\n");
    return 0;
}

void exit_hello(void)
{
    printk(KERN_ALERT "bye~ world\n");
}

module_init(init_hello);
module_exit(exit_hello);
```

此外，大家也应该注意到了，我们使用的打印函数不是glibc提供的printf等，而是内核提供的printk，其中前面的`KERN_ALERT`表示消息级别，是内核定义的一个常量字符串，表示日志级别的，类似的常量在`linux/kern_level.h`里都有定义。

之所以使用`KERN_ALERT`是因为它的日志级别很高，而且在`/proc/sys/kernel/printk`里缺省的console打印的日志级别是(在内核代码的`kernel/printk/printk.c`里定义了`CONSOLE_LOGLEVEL_DEFAULT`等值)是低于`KERN_ALERT`的(注意，数值越小级别越高)，因此可以在控制台下打印出`KERN_ALERT`级别的日志来。

在这里要注意下，在X的模拟终端下是无法打印出来的，所以需要通过`Ctrl-Alt-F2`切换到控制台下。如果你使用的是其它级别的日志，只要高于procfs里设置的终端日志级别，都是可以打印在控制台下的。同时，通过`dmesg`命令或者`cat /var/log/kern.log`文件等也可以看到这些日志。其中，`dmesg`还会根据日志级别标上不同的颜色，非常贴心哦。

最后三行MODULE_开头的宏分别定义了本内核模块的许可证、作者姓名与描述。这些信息都可以在模块加载后，通过运行`modinfo hello.ko`命令看到。另外，需要注意的是由于本内核模块放置在了用户目录，而不是`/lib/modules`目录下，因此直接`modinfo hello`是会失败的。此外，从`modinfo`输出的vermagic信息中就可以看到内核模块编译的内核环境，如果将编译好的内核模块放在一个不同的内核下安装，则会产生类似`Invalid module format (-1): Exec format error`这样的错误信息，此时解决问题的唯一方法就是需要得到驱动/内核模块的源代码，重新进行编译才行。这也就是为什么很多软件虽然提供deb包，但是却没有dkms的原因，因为版本太多了，还不如给用户源代码自己编译呢。

另外还有一点值得注意的是如果你没有设置许可证，或者设置的许可证不是GPL和其它兼容的开源许可证，则在载入此模块的时候会在日志里显示一个警告，即内核已经被污染(kernel tainted)了。

下面我们来聊下Makefile。

如果你尝试过就知道，使用`gcc hello.c`是无法编译出内核模块的，因为无法找到对应的内核头文件，其实即使找到了头文件也会无法链接。因此内核模块的编译总是需要写一个Makefile，而不是用gcc直接编译的。

Makefile的第一行引用了一个obj-m变量，这个就是你需要编译的.o文件。第三行定义了一个内核头文件目录，即`/lib/modules/$(shell uname -r)/build`，此目录实际上是一个软链接，指向的是你内核头文件的所在目录，因此，需要首先安装内核头文件。而下面的all与clean这两个target实际上都是调用了make命令，首先make会切换到(`-C`)kdir目录，使用那个目录下的Makefile编译，然后再切换回来(`M=$(PWD)`)继续本地的编译，而modules和clean则是内核头文件目录的Makefile定义的两个target了，分别表示编译内核模块与清除编译中间与结果文件。

至此，内核模块hello world的源码就剖析完了，下面我们开始为内核模块添加一些基本功能。

## 传递参数

在用户态的程序里，我们往往可以通过命令行向程序传递参数，并通过argc与argv访问命令行参数，那么在内核模块里，我们又如何获取用户传递的参数呢？

[hello world源码](https://bitbucket.org/raphaelzhang/lkm/raw/e8532d/hello/hello.c)的代码演示了如何通过insmod传递参数给内核模块。

在这里，要注意代码中关于name变量的声明和使用。特别是第5行与第6行代码。在这两行代码里，本内核模块将内部的变量声明为可修改的参数，并设定了参数的权限，在例子里此参数可以为root读写，并可以为其它任何用户所读取。这样这个参数就可以在用户态被设置了，首先是可以在载入模块时设置，就像用户态的命令行参数一样，其次可以通过sysfs设置，这样在运行时也可以设置了。如下：

1. 通过`sudo insmod hello.ko name=deepin`命令载入内核模块，并将name参数的值设置为deepin，此时可以看到控制台下打印的将是hello deepin!
2. 运行`sudo modinfo hello.ko`可以发现多了一行parm: name的参数描述
3. 运行`cat /sys/module/hello/parameter/name`命令确认hello内核模块的name参数为deepin
4. 运行`echo -n "China" > /sys/module/hello/parameter/name`命令将hello模块的name参数设置为China，注意，使用-n参数是为了避免出现换行，而且由于参数的权限是644，因此需要切换到root执行此命令
5. 运行`sudo rmmod hello`卸载模块，并注意显示的信息将是bye~ China!

除了charp表示char的指针外，常见的很多C语言的数据类型都支持，例如int、bool、long等。此外，还可以通过`module_param_array`定义数组传参。这些宏定义和参数类型定义都在`linux/moduleparam.h`头文件中可以找到。此外提一句，如果没有第6行，参数就无法通过modinfo看到了，但是仍然可以通过`insmod`的命令行初始化，并使用sysfs设置与读取的。

当然，有时候，我们还需要对参数设置进行一些额外的处理。例如我们有一个变量表示交通灯信号，有绿黄红三色。在程序内部，我们一般是通过enum类型或者int类型表示的，但是对外，我们希望能让用户看到green、yellow与red字样，也最好能让用户通过这三个字符串常量来设置参数，而不是通过0、1、2这样不好记忆的数字来设置。这时候，`module_param`就不好用了，我们可以用`module_param_cb`来设置参数(实际上`module_param`就是对`module_param_cb`的一个简化封装)。参见[hello world源码](https://bitbucket.org/raphaelzhang/lkm/raw/cd9099/hello/hello.c)。

在上面的代码中可以看到，我们增加4~49行的代码，增加了light变量，而这个light变量对外的参数名则是traffic\_light，设置此变量是通过`set_light`函数来设置的，读取此变量是通过`get_light`来设置的。在这两个函数里的kernel\_param类型参数对应的就是我们模块内部的参数/变量，而`set_light`的第一个参数则是外部调用者传进来的字符串，`get_light`的第一个参数则是我们需要传给外部调用者的字符串。

在上面的代码里，我们使用了C99的语法来声明变量，即没有在函数开始就声明了所有变量，而是即用即声明，这样我们还需要修改下Makefile，增加相应的编译选项，代码参见[Makefile源码](https://bitbucket.org/raphaelzhang/lkm/raw/cd9099/hello/Makefile)。

注意以上Makefile内容中仅增加了ccflags-y变量的声明。

我们可以测试如下：

1. 通过`sudo insmod hello.ko name=deepin traffic_light=red`命令载入内核模块，并将name参数的值设置为deepin，将traffic\_light参数的值设置为red，此时可以看到控制台下打印的将是traffic-light: red与hello deepin!
2. 运行`sudo modinfo hello.ko`可以发现多了一行parm: traffic_light的参数描述
3. 运行`cat /sys/module/hello/parameter/traffic_light`命令确认hello内核模块的traffic\_light参数为green yellow [red]
4. 由于参数的权限是644，因此需要切换到root来运行`echo -n "yellow" > /sys/module/hello/parameter/traffic_light`命令将hello模块的traffic\_light参数设置为yellow，注意，使用-n参数是为了避免出现换行。控制台下将打印出traffic-light: yellow
5. 运行`sudo rmmod hello`卸载模块

顺便说一下，源码中使用`module_param`设定name参数的第52行代码等效于如下使用`module_param_cb`实现的代码。

```C
struct kernel_param_ops name_ops = {
    .get = param_get_charp,
    .set = param_set_charp,
};
module_param_cb(name, &name_ops, &name, 0644);
```

其中`param_get_charp`与`param_set_charp`是内核提供的辅助函数，用来读取和设置char*类型的变量/参数的。

## 使用procfs通讯

以上传递参数的方法已经相当不错了，你可以在载入模块的时候初始化参数，可以通过sysfs传递参数，也可以在设置参数的时候进行处理与响应，还可以通过命令行来读取与设置参数。

但是对于大规模的数据传输来说，上述方法不仅无法传递二进制数据(任何参数的传递都必须使用字符串)，而且传输量较小。此外，从上面的代码也可以看到所有的数据访问都已经被封装了一层，即上述的内核模块代码在读取与写入时都没有访问用户态的内存地址空间，都是对经过转换后得到的内核态内存地址空间操作的。这虽然使得开发简单了，但是同时效率也下降了。

一般来说，我们更偏向于用procfs与udev/devfs而不是sysfs进行数据/参数的传递。其中，通过设备节点，即udev/devfs来传输数据更偏向于设备驱动程序，感兴趣的同学可以读读参考里面的资料来学习如何创建一个虚拟的设备，而procfs更常见于内核模块的数据传递，这两种参数传递方式都可以用来传递大量数据(包括二进制数据)。

下面，我们将主要描述如何使用procfs节点进行内核模块与用户态程序的通讯。

要通过procfs通讯，首先我们必须要创建一个/proc下的节点，在当前的Linux内核里，创建procfs节点的函数为`proc_create`，在`linux/proc_fs.h`头文件里声明。我们还是先看演示代码好了，参见[dgst源码](https://bitbucket.org/raphaelzhang/lkm/raw/019710/dgst/dgst.c)。

在这个例子里，我们使用了`module_init`等函数设定初始化与清除函数，这也是内核模块程序里更常用的方法。

在这里的源码里，我们可以看到在载入模块的时候模块创建了一个名为dgst的procfs节点，但是这个节点没有提供任何访问接口(因为其`file_operations`类型的参数里没有设定任何访问函数)，因此实际上这个节点无法提供任何数据，外界程序也无法通过这个节点写入任何数据。在模块载入后，你可以看到`/proc`目录下有了一个dgst节点，但是你无论是cat读取或者echo写入`/proc/dgst`都会产生IO错误。还需要注意的是模块在卸载的时候调用了`remove_proc_entry`函数删除了此节点。

想要在用户态通过procfs读取内核模块的数据或者向内核模块写入数据，我们一般有两种方法：

* 通过文件的读写操作，即通过open/read/write/close等系统接口来打开procfs对应的文件来读写数据
* 通过文件的ioctl操作，即通过open/ioctl/close来打开procfs对应的文件，然后向其通过ioctl系统调用接口发送控制命令

在上面dgst的代码里，我们已经看到了要提供某个procfs节点的数据访问方式，可以通过`file_operations`结构体来设置，内核模块可以通过这个结构体提供procfs节点的打开、读取、写入、关闭、ioctl等的接口函数，以供用户态程序调用。

但是添加读写功能之前，我们需要首先对用户态的文件读写方式有所了解。从用户态读取文件，典型的代码如下所示。

```C
if ((fd = open(filename, O_RDONLY)) >= 0) {
    char buf[4096];
    while ((bytes = read(fd, buf, sizeof buf)) > 0) {
        // do buf processing
    }
    close(fd);
}
```

根据man手册说明，程序调用read成功后，成功读取到的字节数会被返回(0表示到达了文件尾)，而且文件的偏移量(file position)也会被增加这么多字节数，出错的时候应该返回-1，而且相应的errno(如EAGAIN等)也会被设置，以便调用者获取具体的程序错误。而write也有类似的返回描述。

也就是说，我们实现的procfs节点的read与write接口需要处理的参数应该有一个文件、一个用户提供的用来读/写的缓存区、一个缓存区的大小、一个文件偏移量以及一个返回值，而且返回值的处理应该包括errno的处理。

在下面，我们就根据上述read/write的要求为内核模块添加procfs的读写功能，参见[dgst源码](https://bitbucket.org/raphaelzhang/lkm/raw/1ef333/dgst/dgst.c)。我们先介绍一下模块的功能，此模块将对用户输入的数据进行操作，得到一个固定长度的摘要(digest，缩写为dgst)，类似MD5与SHA系列的摘要算法，不过当然为了简单起见，这个摘要算法超级简单，仅仅是字节异或而已。

相较于之前的代码，我们主要新增了11~61行的代码，为procfs添加了读取摘要(`read_dgst`)与写入数据(`write_data`)的接口函数，但是没有提供打开和关闭的接口函数。

从接口函数中可以看到我们之前说的所有参数，包括文件、用户态传进来的缓存区、缓存区大小以及文件偏移量，但是没有errno。实际上glibc对系统调用的返回值进行了处理，当返回值小于0时，glibc会将其转换为-1，并使用返回值的绝对值作为errno，因此在出现缓存区访问错误时都需要返回-EFAULT。

需要注意的是偏移量是内核模块自己维护的，可以根据偏移量的当前值判断整个文件写入到什么地方或者读取到什么地方了。在上述代码中，我们仅在读取摘要的时候设定了偏移量，并在读取摘要的函数里根据设置的偏移量判断是否已经读取过摘要了，如果读取过摘要就不能再读取，不然cat每次调用read都会有数据，从而对同一个摘要会打印无限次。

此外，一定要注意，内核不能直接访问用户态的数据(因为没有经过可访问性检查)，内核态从用户态复制数据可以使用`copy_from_user`或者`get_user`等，内核态向用户态复制数据可以使用`copy_to_user`或者`put_user`等，这些宏通过`linux/uaccess.h`导入即可。而内核态分配与释放内存使用`kmalloc`与`kfree`，它们通过`linux/slab.h`导入即可。

在以上方法中，我们加载模块后可以通过`echo`命令向`/proc/dgst`写入数据，通过`cat`命令从`/proc/dgst`读取摘要，从而证明了我们实现的读写函数已经成功。在`read_dgst`函数里，我们没有返回实际的摘要值，而是摘要值的十六进制可打印字符串形式，主要是为了使用`cat /proc/dgst`的方法来直接看到结果方便调试。

可能已经有同学注意到了，我们实现的函数里没有open与close对应的接口，这是因为在procfs里这两个接口都是可以为空的，具体的实现可以参见内核代码树里`fs/proc/inode.c`与`fs/proc/generic.c`的源码。

当然，没有open与close接口在这里实际上是一个bug，因为这样所有向此设备输出的数据都会影响到全局唯一的一个摘要。一般情况下，我们使用摘要的时候希望的是每次输入一个字符串计算出一个摘要，而不要受到之前使用过摘要函数的影响，这样，就必须实现对应的open与close函数了，参见[dgst源码](https://bitbucket.org/raphaelzhang/lkm/raw/6ecfce/dgst/dgst.c)。

从上述代码可以看出我们的open函数主要就是通过`struct file`的`private_data`成员(void*类型)保存了自己的摘要信息，然后在read/write函数里面就不再需要全局变量，而是访问`private_data`里面的摘要信息了，显然在这里read/write函数需要事先检查下`private_data`的有效性，不然应该返回-EBADF。当然，最后在release函数里需要释放对应在open里分配的内存。

此时，我们已经无法通过`echo`与`cat`来更新查看`/proc/dgst`的数据了，因为每次`echo`与`cat`命令都会打开`/proc/dgst`然后再关闭这个文件，从而使得对文件的更改在关闭后就丢失了。因此我们需要一个用户态的测试程序，代码参见[dgst_userland.c源码](https://bitbucket.org/raphaelzhang/lkm/raw/6ecfce/dgst_userland/dgst_userland.c)。

此测试程序会以读写的方式(O_RDWR)打开`/proc/dgst`文件，先写入一段字符串，然后再读取摘要，程序相当简单，就不再赘述了。

当然，内核模块的`read_dgst`的实现使得它只能被调用一次，这样是为了方便通过`cat /proc/dgst`查看原始的摘要种子值(seed)，不然运行`cat /proc/dgst`就会永无止境了，如果用户态程序已经考虑到了这个问题，那就可以在`read_dgst`里把对offset参数的处理都丢掉了。

## 结语

Linux内核模块涉及到的方方面面还是很多的，我们上面讲的只是一个入门简介。其实如果感兴趣，至少还可以深入了解下：

* 用户态与内核态通讯的其它方式，包括ioctl、seq\_file、sysctl、netlink、系统调用等，其中ioctl就很适合代替上述的`read_dgst`函数，netlink是扩展性最好，性能最高的一种，而添加系统调用需要修改整个内核，对于内核模块来说是不可行的
* 内核态访问用户态的信息，如当前进程的用户名、进程信息、访问文件等
* 内核态模块之间的通讯，包括通过`EXPORT_SYMBOL`的方式直接调用其它模块的函数，或者通过`kallsyms_lookup_name`的方式通过函数名查找内核函数指针间接调用
* 中断处理与相关的tasklet、工作队列(work queue)等
* 内核线程(kernel thread)与锁机制(mutex、semaphore、spinlock等)
* 内核模块的调试，如使用printk、debugfs、串口调试、kdb、kdump/crash等方式的调试

另外，由于内核变化很快，而内核的函数又不固定(不像Windows驱动接口那样稳定)，因此很多网上的资料都过时很快。比如现在网上最常见的仍然是2.6时期的各种内核模块开发资料，虽然总体来说仍然是没有问题的，但是细节上改变仍然很多，举几个2.6.32到3.16以上内核变更的例子：

* 原`__devinit`、`__devexit`宏都消失了，会导致新内核下原代码的编译错误
* 原内核信号量的初始化宏为`init_MUTEX`等，现在已经消失了，需要使用`sema_init`
* 原procfs创建节点的函数是`proc_create_read_entry`等，在新版本的内核里需要使用`proc_create`，当然，也需要修改相应的参数。此外，也需要对原有的参数进行结构体封装
* 文件系统操作结构体(`struct file_operations`)的回调函数有了更改，原来的ioctl改名为unlocked_ioctl，以提高性能，对于用户态与内核态的通讯来说，使用此方法的性能将会更高，当然，函数的参数也发生了变化
* 旧版本内核里的头文件`linux/smp_lock.h`已经被取消(由于BKL已经被清除)，需要换成新版本内核的`linux/hardirq.h`
* ……

上述程序在Deepin Server 15/15.1与Deepin Desktop 15.3上编译测试通过，内核版本分别是3.16与4.4，谨此提醒一下。

还有一点，由于内核接口的不稳定性，而且内核模块载入需要匹配vermagic的关系，内核模块还是尽量少写为好，以避免适配不同版本与分支的内核不停地编译与发布。很多任务其实不需要内核模块就可以搞定的，就不要自己开发内核模块了，比如系统参数调整，比如新的文件系统(可以用FUSE)、比如USB驱动(可以用libusb)。

好了，到这里，就到这里吧，Happy Hacking, Guys ;)

## 参考

* [The Linux Kernel Module Programming Guide](http://www.tldp.org/LDP/lkmpg/2.6/html/)
* [黑客内核：编写属于你的第一个Linux内核模块](https://linux.cn/article-3251-1.html)
* [在Linux下用户空间与内核空间数据交换的方式](https://www.ibm.com/developerworks/cn/linux/l-kerns-usrs/) [2](https://www.ibm.com/developerworks/cn/linux/l-kerns-usrs2/)
* Linux Kernel Source