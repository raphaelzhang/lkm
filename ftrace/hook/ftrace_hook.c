#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/ftrace.h>
#include <linux/fs.h>
#include <linux/kallsyms.h>

// ref: https://github.com/ilammy/ftrace-hook/blob/master/ftrace_hook.c

struct ftrace_hook {
	unsigned long orig_func;
	unsigned long my_func;
	struct ftrace_ops fops;
} fhook;

static int my_vfs_create(struct inode *dir, struct dentry *dentry, umode_t mode,
		bool want_excl)
{
	static int (*orig_vfs_create)(struct inode *dir, struct dentry *dentry, umode_t mode,
		bool want_excl) = 0;
	if (orig_vfs_create == 0)
		*((unsigned long *)&orig_vfs_create) = fhook.orig_func + MCOUNT_INSN_SIZE;
	char buf[256];
	char *path = dentry_path_raw(dentry, buf, sizeof(buf));
	if (IS_ERR(path)) {
		pr_info("vfs-create called\n");
	} else if (strstr(path, "123456")) {
		pr_info("vfs-create: %s REJECTED\n", path);
		return -EPERM;
	} else {
		pr_info("vfs-create: %s\n", path);
	}

	int ret = orig_vfs_create(dir, dentry, mode, want_excl);
	if (IS_ERR(path))
		pr_info("vfs-create ret: %d\n", ret);
	else
		pr_info("vfs-create: %s, ret: %d\n", path, ret);
	return ret;
}

static void notrace my_ftrace_func(unsigned long ip, unsigned long parent_ip,
		struct ftrace_ops *ops, struct pt_regs *regs)
{
	struct ftrace_hook *hook = container_of(ops, struct ftrace_hook, fops);
	regs->ip = hook->my_func;
}

int __init init_module()
{
	fhook.orig_func = kallsyms_lookup_name("vfs_create");
	if (fhook.orig_func == 0) {
		pr_err("kallsyms-lookup-name failed\n");
		return -1;
	}
	fhook.my_func = (unsigned long)&my_vfs_create;
	fhook.fops.func = my_ftrace_func;
	fhook.fops.flags = FTRACE_OPS_FL_SAVE_REGS | FTRACE_OPS_FL_IPMODIFY | FTRACE_OPS_FL_RECURSION_SAFE;

	int ret = ftrace_set_filter_ip(&fhook.fops, fhook.orig_func, 0, 0);
	if (ret) {
		pr_err("ftrace-set-filter-ip error: %d\n", ret);
		return ret;
	}

	ret = register_ftrace_function(&fhook.fops);
	if (ret) {
		pr_err("reg-ftrace-func error: %d\n", ret);
		ftrace_set_filter_ip(&fhook.fops, fhook.orig_func, 1, 0);
		return ret;
	}

	pr_info("reg-ftrace-hook done\n");
    return 0;
}

void __exit cleanup_module()
{
	int ret = unregister_ftrace_function(&fhook.fops);
	if (ret) {
		pr_err("unreg-ftrace-func error: %d\n", ret);
		return;
	}
	ret = ftrace_set_filter_ip(&fhook.fops, fhook.orig_func, 1, 0);
	if (ret) {
		pr_err("ftrace-set-filter-ip error: %d\n", ret);
		return;
	}

	pr_info("unreg-ftrace-hook done\n");
}

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Raphael");
MODULE_DESCRIPTION("ftrace-hook demo module");
