#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/ftrace.h>
#include <linux/fs.h>

#define TRACE_DO_FILP_OPEN 1

#ifdef TRACE_DO_FILP_OPEN

#include <linux/kallsyms.h>

static unsigned long get_arg(struct pt_regs* regs, int n)
{
/*
	x86_64 args order: rdi, rsi, rdx, rcx, r8, r9, then comes the stack
	loongson3 64bit args order: a0, a1, a2, a3, which is regs[4] ~ regs[7], then comes the stack
	PT_R4(32) is the offset of regs[4] for loongson 64-bit, quoted from asm-offsets.h
*/
	switch (n) {
#if	defined(CONFIG_X86_64)

		case 1:	return regs->di;
		case 2: return regs->si;
		case 3: return regs->dx;
		case 4: return regs->cx;
		case 5: return regs->r8;
		case 6: return regs->r9;

#elif defined(CONFIG_CPU_LOONGSON3)

		case 1:  // a0
		case 2:  // a1
		case 3:  // a2
		case 4:  // a3
			return *(unsigned long*)((char *)regs + (3+n)*8);

#endif // CONFIG_X86_64
		default:
			return 0;
	}
	return 0;
}

#endif

static void notrace my_ftrace_func(unsigned long ip, unsigned long parent_ip,
		struct ftrace_ops *ops, struct pt_regs *regs)
{
#ifdef TRACE_DO_FILP_OPEN
	struct filename* fn = (struct filename*)get_arg(regs, 2);
	pr_info("%pf called from %pf: %s\n", (void *)ip, (void *)parent_ip, fn->name);
#else
	char self[128] = {}, parent[128] = {};
	sprint_symbol_no_offset(parent, parent_ip);
	sprint_symbol_no_offset(self, ip);
	pr_info("%s called from %s\n", self, parent);
#endif
}

static struct ftrace_ops fops = {
	.func = my_ftrace_func,
	.flags = FTRACE_OPS_FL_SAVE_REGS,
};

int __init init_module()
{
#ifdef TRACE_DO_FILP_OPEN
	char* fname = "do_filp_open";
#else
	char* fname = "*_permission";
#endif
	int ret = ftrace_set_filter(&fops, fname, strlen(fname), 0);
	if (ret) {
		pr_err("ftrace-set-filter error: %d\n", ret);
		return ret;
	}

	ret = register_ftrace_function(&fops);
	if (ret) {
		pr_err("reg-ftrace-func error: %d\n", ret);
		return ret;
	}

	pr_info("reg-ftrace-peek done\n");
    return 0;
}

void __exit cleanup_module()
{
	unregister_ftrace_function(&fops);
	pr_info("unreg-ftrace-peek done\n");
}

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Raphael");
MODULE_DESCRIPTION("ftrace-peek demo module");
