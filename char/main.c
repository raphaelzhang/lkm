#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void usage(char* prog)
{
	printf("usage:\n");
	printf("\tread async/blocking from device: `%s read /dev/XXX A|B`\n", prog);
	printf("\twrite $string async/blocking to device: `%s write /dev/XXX A|B $string`\n", prog);
}

void try_read(char* argv[])
{
	if (strcmp(argv[0], "read")) {
		printf("verb should be read\n");
		return;
	}

	if (strcmp(argv[2], "A") && strcmp(argv[2], "B")) {
		printf("mode should be A(async) or B(blocking)\n");
		return;
	}

	char buf[1<<10] = {};
	int fd = open(argv[1], O_RDONLY | (strcmp(argv[2], "A") == 0 ? O_NONBLOCK : 0));
	if (fd < 0) {
		printf("error open %s: %m\n", argv[1]);
		return;
	}

	ssize_t n = read(fd, buf, sizeof(buf));
	close(fd);

	printf("read[%lld]: %s\n", n, buf);
}

void try_write(char* argv[])
{
	if (strcmp(argv[0], "write")) {
		printf("verb should be write\n");
		return;
	}

	if (strcmp(argv[2], "A") && strcmp(argv[2], "B")) {
		printf("mode should be A(async) or B(blocking)\n");
		return;
	}

	int fd = open(argv[1], O_WRONLY | (strcmp(argv[2], "A") == 0 ? O_NONBLOCK : 0));
	if (fd < 0) {
		printf("error open %s: %m\n", argv[1]);
		return;
	}

	ssize_t n = write(fd, argv[3], strlen(argv[3])+1);
	close(fd);

	printf("write[%lld]: %s\n", n, argv[3]);
}

int main(int argc, char* argv[])
{
	if (argc == 4)
		try_read(argv+1);
	else if (argc == 5)
		try_write(argv+1);
	else
		usage(argv[0]);
}
