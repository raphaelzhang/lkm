#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>

#define DEV_NAME "my_char_dev"
#define MEM_SIZE (1<<20)

static struct my_dev_t {
	struct cdev cdev;
	char memory[MEM_SIZE];
	int memory_size;
	struct mutex lock;
	wait_queue_head_t read_wq;
	wait_queue_head_t write_wq;
} my_dev;

static dev_t my_dev_no;

static ssize_t my_dev_read(struct file* filp, char __user* buf, size_t size, loff_t* offset)
{
	DECLARE_WAITQUEUE(wait, current);
	mutex_lock(&my_dev.lock);
	add_wait_queue(&my_dev.read_wq, &wait);

	int ret = 0;
	while (my_dev.memory_size == 0) {
		if (filp->f_flags & O_NONBLOCK) {
			ret = -EAGAIN;
			goto out;
		}

		__set_current_state(TASK_INTERRUPTIBLE);
		mutex_unlock(&my_dev.lock);

		schedule();
		if (signal_pending(current)) {
			ret = -ERESTARTSYS;
			goto out2;
		}

		mutex_lock(&my_dev.lock);
	}

	if (size > my_dev.memory_size)
		size = my_dev.memory_size; 

	if (copy_to_user(buf, my_dev.memory, size)) {
		ret = -EFAULT;
		goto out;
	} else {
		memcpy(my_dev.memory, my_dev.memory + size, my_dev.memory_size - size);
		my_dev.memory_size -= size;
		wake_up_interruptible(&my_dev.write_wq);
		ret = size;
	}

out:
	mutex_unlock(&my_dev.lock);
out2:
	remove_wait_queue(&my_dev.read_wq, &wait);
	set_current_state(TASK_RUNNING);
	return ret;
}

static ssize_t my_dev_write(struct file* filp, const char __user* buf, size_t size, loff_t* offset)
{
	DECLARE_WAITQUEUE(wait, current);
	mutex_lock(&my_dev.lock);
	add_wait_queue(&my_dev.write_wq, &wait);

	int ret = 0;
	while (my_dev.memory_size == MEM_SIZE) {
		if (filp->f_flags & O_NONBLOCK) {
			ret = -EAGAIN;
			goto out;
		}

		__set_current_state(TASK_INTERRUPTIBLE);
		mutex_unlock(&my_dev.lock);

		schedule();
		if (signal_pending(current)) {
			ret = -ERESTARTSYS;
			goto out2;
		}

		mutex_lock(&my_dev.lock);
	}

	if (size + my_dev.memory_size > MEM_SIZE)
		size = MEM_SIZE - my_dev.memory_size; 

	if (copy_from_user(my_dev.memory + my_dev.memory_size, buf, size)) {
		ret = -EFAULT;
		goto out;
	} else {
		my_dev.memory_size += size;
		wake_up_interruptible(&my_dev.read_wq);
		ret = size;
	}

out:
	mutex_unlock(&my_dev.lock);
out2:
	remove_wait_queue(&my_dev.write_wq, &wait);
	set_current_state(TASK_RUNNING);
	return ret;
}

static struct file_operations my_dev_fops = {
	.owner = THIS_MODULE,
	.read = my_dev_read,
	.write = my_dev_write,
	.llseek = default_llseek
};

// create device with `mknod /dev/my_char_dev c #major 0`, where you can find #major in /proc/devices
// remove device with `rm /dev/my_char_dev`
static int __init init_dev(void)
{
	alloc_chrdev_region(&my_dev_no, 0, 1, DEV_NAME);

	cdev_init(&my_dev.cdev, &my_dev_fops);
	my_dev.cdev.owner = THIS_MODULE;
	int ret = cdev_add(&my_dev.cdev, my_dev_no, 1);
	if (ret) {
		pr_err("cdev-add failed: %d\n", ret);
		return ret;
	}
	mutex_init(&my_dev.lock);
	memset(my_dev.memory, 0, MEM_SIZE);
	init_waitqueue_head(&my_dev.read_wq);
	init_waitqueue_head(&my_dev.write_wq);
	my_dev.memory_size = 0;
	pr_info("my-dev reg ok\n");
    return 0;
}

static void __exit exit_dev(void)
{
	cdev_del(&my_dev.cdev);
	unregister_chrdev_region(my_dev_no, 1);
	pr_info("my-dev unreg ok\n");
}

module_init(init_dev);
module_exit(exit_dev);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Raphael");
MODULE_DESCRIPTION("Char Device Kernel Module");
