#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "dgst.h"

int test_dgst(int fd, int argc, char* argv[])
{
    char *buf = argc == 1 ? "hello, world" : argv[1];
    if (write(fd, buf, strlen(buf)) <= 0) {
        perror("write");
        close(fd);
        return -2;
    }

    unsigned char raw[16];
    if (ioctl(fd, DGST_IOCTL_READ, raw) != 0) {
        perror("ioctl");
        close(fd);
        return -3;
    }
    printf("digest raw:");
    for (int i = 0; i < DGST_SIZE; i++)
        printf(" 0x%02X", raw[i]);
    printf("\n");

    char result[32] = {0};
    if (read(fd, result, sizeof result) <= 0) {
        perror("read");
        return -4;
    } else {
        printf("digest of '%s' is %s\n", buf, result);
    }
    return 0;
}

int main(int argc, char* argv[])
{
    int fd = open("/proc/dgst", O_RDWR);
    if (fd < 0) {
        perror("open");
        return -1;
    }

    int ret = 0;
    if ((ret = test_dgst(fd, argc, argv)) != 0)
        return ret;

    if (argc > 1 && ioctl(fd, DGST_IOCTL_RESET) != 0) {
        perror("ioctl2");
        close(fd);
        return -5;
    }

    if ((ret = test_dgst(fd, argc, argv)) != 0)
        return ret;

    close(fd);
}
