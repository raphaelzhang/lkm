#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/ioctl.h>

#include "dgst.h"

static struct proc_dir_entry* procfs_entry = 0;
static unsigned char seed[DGST_SIZE] = {0x6c, 0xb3, 0x47, 0x93, 0x21, 0x78, 0xdb, 0x2f};

struct dgst_metadata {
    unsigned int dgst_offset;
    unsigned char dgst[DGST_SIZE];
};

static int open_dgst(struct inode* si, struct file* sf)
{
    struct dgst_metadata* dm = kzalloc(sizeof(struct dgst_metadata), GFP_KERNEL);
    if (dm == 0)
        return -ENOMEM;

    memcpy(dm->dgst, seed, DGST_SIZE);
    sf->private_data = dm;
    return 0;
}

static int close_dgst(struct inode* si, struct file* sf)
{
    if (sf == 0 || sf->private_data == 0)
        return -EBADF;

    kfree(sf->private_data);
    sf->private_data = 0;
    return 0;
}

static ssize_t read_dgst(struct file* sf, char __user* buf, size_t size, loff_t* offset)
{
    if (sf == 0 || sf->private_data == 0)
        return -EBADF;

    if (*offset != 0)
        return 0;

    if (size < DGST_SIZE*2 + 1)
        return -EINVAL;

    struct dgst_metadata* dm = (struct dgst_metadata*)sf->private_data;

    char tmp[32];
    snprintf(tmp, sizeof tmp, "%02x%02x%02x%02x%02x%02x%02x%02x", dm->dgst[0], dm->dgst[1], dm->dgst[2], 
        dm->dgst[3], dm->dgst[4], dm->dgst[5], dm->dgst[6], dm->dgst[7]);
    unsigned int len = strlen(tmp)+1;
    if (copy_to_user(buf, tmp, len) == 0) {
        *offset = len;
        return len;
    }
    return -EFAULT;
}

static long ioctl_dgst(struct file* sf, unsigned int cmd, unsigned long arg)
{
    if (sf == 0 || sf->private_data == 0)
        return -EBADF;

    struct dgst_metadata* dm = (struct dgst_metadata*)sf->private_data;

    // unsigned int cmd_nr = _IOC_NR(cmd);
    switch (cmd) {
    case DGST_IOCTL_READ:
        if (copy_to_user((char *)arg, dm->dgst, DGST_SIZE) != 0)
            return -EFAULT;
        break;
    case DGST_IOCTL_RESET:
        memcpy(dm->dgst, seed, DGST_SIZE);
        dm->dgst_offset = 0;
        break;
    default:
        pr_err("Unrecognized ioctl cmd: %d\n", cmd);
        return -EINVAL;
    }
    return 0;
}

static ssize_t write_data(struct file* sf, const char __user* buf, size_t size, loff_t* offset)
{
    if (sf == 0 || sf->private_data == 0)
        return -EBADF;

    if (size == 0) return 0;

    char* tmp = kmalloc(size, GFP_KERNEL);
    if (tmp == 0)
        return -ENOMEM;

    if (copy_from_user(tmp, buf, size) != 0) {
        kfree(tmp);
        return -EFAULT;
    }

    struct dgst_metadata* dm = (struct dgst_metadata*)sf->private_data;
    for (unsigned int i = 0; i < size; i++) {
        unsigned int pos = (dm->dgst_offset + i) % DGST_SIZE;
        dm->dgst[pos] = dm->dgst[pos] ^ tmp[i];
    }
    dm->dgst_offset = (dm->dgst_offset + size) % DGST_SIZE;
    kfree(tmp);
    return size;
}

static struct file_operations procfs_ops = {
    .owner = THIS_MODULE,
    .open = open_dgst,
    .read = read_dgst,
    .write = write_data,
    .unlocked_ioctl = ioctl_dgst,
    .release = close_dgst,
};

static int __init init_dgst(void)
{
    procfs_entry = proc_create(PROCFS_NAME, 0666, 0, &procfs_ops);
    return 0;
}

static void __exit exit_dgst(void)
{
    if (procfs_entry != 0) {
        remove_proc_entry(PROCFS_NAME, 0);
        procfs_entry = 0;
    }
}

module_init(init_dgst);
module_exit(exit_dgst);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Raphael");
MODULE_DESCRIPTION("ProcFS Demo Kernel Module");
