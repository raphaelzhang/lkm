#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/livepatch.h>
#include <linux/fs.h>
#include <linux/fsnotify.h>
#include <linux/dcache.h>

static int livepatch_file_open(struct file* file, const struct cred* cred)
{
    char buf[256] = {0};
    if (file != 0) {
        char* path = dentry_path_raw(file->f_path.dentry, buf, sizeof buf);
        if (strncmp(path, "/home/", 6) == 0) {
            pr_info("file-open: %s\n", path);
            if (strstr(path, "abcd") > 0)
                return -EPERM;
        }
    }
    return fsnotify_perm(file, MAY_OPEN);
}

static struct klp_func funcs[] = {
    {
        .old_name = "security_file_open",
        .new_func = livepatch_file_open,
    }, { }
};

static struct klp_object objs[] = {
    {
        .funcs = funcs,
    }, { }
};

static struct klp_patch patch = {
    .mod = THIS_MODULE,
    .objs = objs,
};

int __init init_module()
{
    int ret = klp_register_patch(&patch);
    if (ret)
        return ret;
    ret = klp_enable_patch(&patch);
    if (ret)
        WARN_ON(klp_unregister_patch(&patch));
    return ret;
}

void __exit cleanup_module()
{
    WARN_ON(klp_disable_patch(&patch));
    WARN_ON(klp_unregister_patch(&patch));
}

MODULE_LICENSE("GPL");
MODULE_INFO(livepatch, "Y");
