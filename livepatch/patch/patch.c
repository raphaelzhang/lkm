#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/livepatch.h>
#include <linux/delay.h>

static void livepatch_msleep(unsigned int msecs)
{
    if (msecs == 0)
        usleep_range(100, 200);
    else
        usleep_range(msecs*1000-50, msecs*1000+50);
}

static struct klp_func funcs[] = {
    {
        .old_name = "msleep",
        .new_func = livepatch_msleep,
    }, { }
};

static struct klp_object objs[] = {
    {
        .funcs = funcs,
    }, { }
};

static struct klp_patch patch = {
    .mod = THIS_MODULE,
    .objs = objs,
};

int __init init_module()
{
    int ret = klp_register_patch(&patch);
    if (ret)
        return ret;
    ret = klp_enable_patch(&patch);
    if (ret)
        WARN_ON(klp_unregister_patch(&patch));
    return ret;
}

void __exit cleanup_module()
{
    WARN_ON(klp_disable_patch(&patch));
    WARN_ON(klp_unregister_patch(&patch));
}

MODULE_LICENSE("GPL");
MODULE_INFO(livepatch, "Y");
