#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/livepatch.h>

/*
 * ported from kernel livepatch sample
 *
 * Example:
 *
 * $ cat /proc/cmdline
 * <your cmdline>
 *
 * # insmod cmdline.ko
 * $ cat /proc/cmdline
 * cmdline livepatched
 * OR
 * patched cmdline: '<your cmdline>'
 *
 * # echo 0 > /sys/kernel/livepatch/cmdline/enabled
 * $ cat /proc/cmdline
 * <your cmdline>
 */

#include <linux/seq_file.h>

static int patched_cmdline(struct seq_file *m, void *v)
{
    static char* cmdline = 0;
    if (!cmdline)
        cmdline = *(char **)kallsyms_lookup_name("saved_command_line");

    seq_printf(m, "patched cmdline: '%s'\n", cmdline);
    return 0;
}

static struct klp_func funcs[] = {
    {
        .old_name = "cmdline_proc_show",
        .new_func = patched_cmdline,
    }, { }
};

static struct klp_object objs[] = {
    {
        .funcs = funcs,
    }, { }
};

static struct klp_patch patch = {
    .mod = THIS_MODULE,
    .objs = objs,
};

int init_module()
{
    int ret = klp_register_patch(&patch);
    if (ret)
        return ret;
    ret = klp_enable_patch(&patch);
    if (ret) {
        WARN_ON(klp_unregister_patch(&patch));
        return ret;
    }
    return 0;
}

void cleanup_module()
{
    WARN_ON(klp_disable_patch(&patch));
    WARN_ON(klp_unregister_patch(&patch));
}

MODULE_LICENSE("GPL");
MODULE_INFO(livepatch, "Y");
