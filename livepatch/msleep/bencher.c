#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/time.h>
#include <linux/jiffies.h>

static unsigned long sleep_usecs = 1000;

int set_sleep_time(const char* val, const struct kernel_param* kp)
{
    int ret = param_set_long(val, kp);
    if (ret < 0) {
        pr_err("err setting sleep-usecs: %s, %d\n", val, ret);
        return ret;
    }

    if (sleep_usecs < 50)
        sleep_usecs = 50;

    struct timeval s_time, e_time;
    do_gettimeofday(&s_time);
    unsigned long s_jiffies = jiffies;

    if (sleep_usecs % 1000 == 0)
        msleep(sleep_usecs / 1000);
    else
        usleep_range(sleep_usecs - 50, sleep_usecs + 50);

    unsigned long e_jiffies = jiffies;
    do_gettimeofday(&e_time);

    unsigned long dur_time = (e_time.tv_usec + e_time.tv_sec*1000000)
        - (s_time.tv_usec + s_time.tv_sec*1000000);
    pr_err("sleep-usecs: %lu, dur-jiffies: %lu, actual usecs: %lu\n", sleep_usecs,
        e_jiffies - s_jiffies, dur_time);
    return ret;
}

struct kernel_param_ops sleep_time_setter = {
    .set = set_sleep_time,
    .get = param_get_long,
};

module_param_cb(sleep_usecs, &sleep_time_setter, &sleep_usecs, 0644);
MODULE_PARM_DESC(sleep_usecs, "Sleep time in microsecond");

int __init init_module()
{
    return 0;
}

void __exit cleanup_module()
{
}

MODULE_LICENSE("GPL");
