#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/livepatch.h>
#include <linux/sched.h>
#include <linux/socket.h>

static int block_sogou_inet_socket(int family, int type, int protocol, int kern)
{
    if (family != AF_INET || kern == 1)
        return 0;

    struct task_struct* ts = current;
    while (ts != 0 && ts->tgid > 1) {
        struct task_struct* leader = ts->group_leader;
        if (!leader)
            break;

        if (strcmp(leader->comm, "sogou-qimpanel") == 0)
            return -EPERM;
        ts = ts->parent;
    }

    return 0;
}

static struct klp_func funcs[] = {
    {
        .old_name = "security_socket_create",
        .new_func = block_sogou_inet_socket,
    }, { }
};

static struct klp_object objs[] = {
    {
        .funcs = funcs,
    }, { }
};

static struct klp_patch patch = {
    .mod = THIS_MODULE,
    .objs = objs,
};

int __init init_module()
{
    int ret = klp_register_patch(&patch);
    if (ret)
        return ret;
    ret = klp_enable_patch(&patch);
    if (ret)
        WARN_ON(klp_unregister_patch(&patch));
    return ret;
}

void __exit cleanup_module()
{
    WARN_ON(klp_disable_patch(&patch));
    WARN_ON(klp_unregister_patch(&patch));
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Raphael");
MODULE_DESCRIPTION("Sogou Cloud Update Blocker");
MODULE_INFO(livepatch, "Y");
