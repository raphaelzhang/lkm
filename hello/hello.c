#ifdef CONFIG_MODVERSION
#define MOD_VERSIONS
#include <linux/modversions.h>
#endif
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/tty.h>

#define HELLO_PREF "[hello] "

static void write_console(const char* fmt, ...)
{
    char msg[1024];
    va_list args;
    va_start(args, fmt);
    vsnprintf(msg, sizeof msg, fmt, args);
    va_end(args);

    if (current != 0 && current->signal != 0 && current->signal->tty != 0) {
       struct tty_struct* tty = current->signal->tty;
       tty->driver->ops->write(tty, HELLO_PREF, strlen(HELLO_PREF));
       tty->driver->ops->write(tty, msg, strlen(msg));
       tty->driver->ops->write(tty, "\n\r", 2);
    } else {
       pr_alert("%s\n", msg);
    }
}

char* lights[] = {"green", "yellow", "red", 0};
int light = 0;  // 0: green, 1: yellow, 2: red

int set_light(const char* val, const struct kernel_param* kp)
{
    int user_light = -1;
    for (int i = 0; lights[i]; i++) {
        if (strcmp(lights[i], val) == 0) {
            user_light = i;
            break;
        }
    }

    if (user_light == -1 || user_light == light)
        return 0;

    write_console("traffic-light updated: '%s' from '%s'", val, lights[light]);
    light = user_light;
    return strlen(val);
}

int get_light(char* buf, const struct kernel_param* kp)
{
    buf[0] = 0;
    for (int i = 0; lights[i]; i++) {
        if (i > 0) strcat(buf, " ");

        if (i == light)
            strcat(buf, "[");

        strcat(buf, lights[i]);

        if (i == light)
            strcat(buf, "]");
    }
    return strlen(buf)+1;
}

struct kernel_param_ops light_switcher = {
    .set = set_light,
    .get = get_light,
};

module_param_cb(traffic_light, &light_switcher, &light, 0644);
MODULE_PARM_DESC(traffic_light, "Traffic light: green yellow red");

char* name = "world";
module_param(name, charp, 0644);
MODULE_PARM_DESC(name, "Name of the user");

int __init init_module()
{
    pr_alert("hello, %s!\n", name);
    return 0;
}

void __exit cleanup_module()
{
    pr_alert("bye~ %s!\n", name);
}

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Raphael");
MODULE_DESCRIPTION("Hello World Kernel Module");
