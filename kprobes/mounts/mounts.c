#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/uaccess.h>
#include <linux/file.h>
#include <linux/slab.h>
#include <linux/namei.h>
#include <linux/stat.h>

#define ALLOC_UNIT	(1<<10)

static char* read_file_content(struct file* filp, int* real_size)
{
	int size = ALLOC_UNIT;
	char* buf = 0;
	while (1) {
		if (buf)
			kfree(buf);
		buf = kmalloc(size, GFP_KERNEL);
		if (unlikely(buf == 0))
			break;

		loff_t off = 0;
		char __user* user_buf = (char __user*)buf;
		*real_size = __vfs_read(filp, user_buf, size, &off);
		if (*real_size > 0 && *real_size < size) {
			buf[*real_size] = 0;
			break;
		}

		size += ALLOC_UNIT;
	}
	return buf;
}

static int mounted_at(const char* mp, const char* root)
{
	return strcmp(mp, root) == 0 || (strlen(mp) > strlen(root) && strstr(mp, root) == mp && mp[strlen(root)] == '/');
}

static int get_mounts(char* buf, char* mounts)
{
	int mnt_count = 0;
	char* line = buf, *p = mounts;
	char mp[NAME_MAX];
	while (sscanf(line, "%*s %s %*s %*s %*d %*d\n", mp) == 1) {
		line = strchr(line, '\n') + 1;

		if (mounted_at(mp, "/sys") || mounted_at(mp, "/proc") ||
			mounted_at(mp, "/run") || mounted_at(mp, "/dev"))
			continue;

		struct path path;
		kern_path(mp, LOOKUP_FOLLOW, &path);
		struct kstat stat;
		vfs_getattr_nosec(&path, &stat);
		*p++ = MAJOR(stat.dev);
		*p++ = MINOR(stat.dev);
		strcpy(p, mp);
		p += strlen(mp)+1;
		mnt_count++;
	}
	return mnt_count;
}

static int get_fs_info(char* mounts)
{
	struct file* filp = filp_open("/proc/mounts", O_RDONLY, 0);
	if (IS_ERR(filp)) {
		pr_err("error openning /proc/mounts\n");
		return 0;
	}
	mm_segment_t old_fs = get_fs();
	set_fs(get_ds());
	
	// i_size_read is useless here because procfs does not have i_size
	// loff_t size = i_size_read(file_inode(filp));
	int real_size = 0;
	char* buf = read_file_content(filp, &real_size);
	set_fs(old_fs);
	fput(filp);
	if (buf == 0)
		return 0;

	pr_info("/proc/mounts size: %d\n", real_size);
	int mnt_count = get_mounts(buf, mounts);
	kfree(buf);
	return mnt_count;
}

static void populate_mounts(void)
{
	char buf[1024];
	int mnt_count = get_fs_info(buf);
	char* p = buf;
	for (int i = 0; i < mnt_count; i++) {
		char major, minor;
		major = *p++;
		minor = *p++;
		pr_info("mp: %s, major: %d, minor: %d\n", p, major, minor);
		p += strlen(p) + 1;
	}
}

/*
static long (const char* dev_name, const char __user* dir_name, const char* type_page,
	unsigned long flags, void* data_page)
*/
static int on_do_mount_ret(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	unsigned long retval = regs_return_value(regs);
	if (retval != 0)
		return 0;
	populate_mounts();
	return 0;
}

static struct kretprobe do_mount_krp = {
	.handler		= on_do_mount_ret,
	.maxactive		= 64,
	.kp.symbol_name = "do_mount",
};

int __init init_module()
{
	populate_mounts();
	int ret = register_kretprobe(&do_mount_krp);
	if (ret < 0) {
		pr_err("register_kretprobe do-mount failed, returned %d\n", ret);
		return -1;
	}
	pr_info("register_kretprobe do-mount ok, kretprobe at %p\n", do_mount_krp.kp.addr);
	return 0;
}

void __exit cleanup_module()
{
	unregister_kretprobe(&do_mount_krp);
	pr_info("unregister_kretprobe do-mount ok\n");
}

MODULE_LICENSE("GPL");
