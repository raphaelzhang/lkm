#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <linux/sched.h>

static int sec_open_callback(struct file* filp, const struct cred* cred)
{
	if (S_ISDIR(filp->f_inode->i_mode))
		goto out;

	char buf[1024];
	char* fn = file_path(filp, buf, sizeof buf);
	if (unlikely(fn == 0))
		goto out;

	if (strstr(fn, "/dev/") == fn || strstr(fn, "/proc/") == fn ||
		strstr(fn, "/tmp/") == fn || strstr(fn, "/sys/") == fn ||
		strstr(fn, "/run/") == fn ||
		strstr(fn, "net:") == fn || strstr(fn, "pipe:") == fn)
		goto out;
	
	pr_info("sec-open-fn: %s comm: %s\n", fn, current ? current->comm : 0);

out:
    jprobe_return();
    return 0;
}

static struct jprobe sec_open_probe = {
    .entry = sec_open_callback,
    .kp = {
        .symbol_name = "security_file_open",
    },
};

int __init init_module()
{
    int ret = register_jprobe(&sec_open_probe);
	if (ret < 0) {
		pr_err("register_jprobe sec-open failed, returned %d\n", ret);
		return -1;
	}
    pr_info("register_jprobe sec-open ok, jprobe at %p, handler addr %p\n",
        sec_open_probe.kp.addr, sec_open_probe.entry);
	return 0;
}

void __exit cleanup_module()
{
    unregister_jprobe(&sec_open_probe);
	pr_info("unregister_jprobe sec-open ok\n");
}

MODULE_LICENSE("GPL");
