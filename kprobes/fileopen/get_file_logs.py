#!/usr/bin/env python

import os

stats = {}
needle = 'sec-open-fn:'
with os.popen("sudo journalctl _TRANSPORT=kernel") as journals:
	for line in journals:
		line = line.strip()
		idx = line.find(needle)
		if idx == -1:
			continue
		line = line[idx+len(needle):].strip()
		fields = line.split(' comm: ')
		fn, comm = fields[0], fields[1]
		stats.setdefault(comm, set([]))
		stats[comm].add(fn)

all_files = set([])
for key in stats.keys():
	# print key
	for ele in stats[key]:
		#print '\t', ele
		all_files.add(ele)

#print '*'
for ele in sorted(all_files):
	print ele
