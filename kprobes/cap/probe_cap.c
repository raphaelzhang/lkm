#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>

static int pre_cap_capable(const struct cred *cred, struct user_namespace *ns,
		       int cap, int audit)
{
    if (current) {
        // char exec_name[TASK_COMM_LEN] = {0};
        // get_task_comm(exec_name, current);
        // pr_info("cap-capable jprobe: %d[%s], %d\n", current->pid, exec_name, cap);
        pr_info("cap-capable jprobe: %d[%s], %d\n", current->pid, current->comm, cap);
    } else {
        pr_err("cap-capable current null\n");
    }
    jprobe_return();
    return 0;
}

static struct jprobe cap_probe = {
    .entry = pre_cap_capable,
    .kp = {
        .symbol_name = "cap_capable",
    },
};

int __init init_module()
{
    int ret = register_jprobe(&cap_probe);
    if (ret < 0) {
        pr_err("register_jprobe failed: %d\n", ret);
        return -1;
    }
    pr_info("register_jprobe ok, jprobe at %p, handler addr %p\n",
        cap_probe.kp.addr, cap_probe.entry);
    return 0;
}

void __exit cleanup_module()
{
    unregister_jprobe(&cap_probe);
}

MODULE_LICENSE("GPL");
