#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>

#include <linux/fs.h>
#include <linux/dcache.h>
#include <linux/sched.h>

struct vfs_create_args {
	struct inode *dir;
	struct dentry *dentry;
};

static unsigned long get_arg(struct pt_regs* regs, int n)
{
	/*
		x86_64 args order: rdi, rsi, rdx, rcx, r8, r9, then comes the stack
		loongson3 64bit args order: a0, a1, a2, a3, which is regs[4] ~ regs[7], then comes the stack
	*/
#ifdef CONFIG_X86_64

	switch (n) {
		case 1:
			return regs->di;
		case 2:
			return regs->si;
		case 3:
			return regs->dx;
		case 4:
			return regs->cx;
		case 5:
			return regs->r8;
		case 6:
			return regs->r9;
		default:
			return 0;
	}

#else // CONFIG_X86_64

#ifdef CONFIG_CPU_LOONGSON3

	switch (n) {
		case 1:  // a0
		case 2:  // a1
		case 3:  // a2
		case 4:  // a3
			return regs->regs[n+3];
		default:
			return 0;
	}

#endif // CONFIG_CPU_LOONGSON3

#endif // CONFIG_X86_64
	return 0;
}

static int on_vfs_create_ent(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	// struct inode *dir, struct dentry *dentry, umode_t mode, bool want_excl	
	struct vfs_create_args *args = (struct vfs_create_args *)ri->data;
	args->dir = (struct inode*)get_arg(regs, 1);
	args->dentry = (struct dentry*)get_arg(regs, 2);
	return 0;
}

static int on_vfs_create_ret(struct kretprobe_instance *ri, struct pt_regs *regs)
{
	struct vfs_create_args *args = (struct vfs_create_args *)ri->data;
	unsigned long retval = regs_return_value(regs);

	if (args->dir == 0 || args->dentry == 0) {
		pr_info("dir: %p, dentry: %p, vfs_create retval: %lu in proc[%d]: %s\n", args->dir, args->dentry, retval, current->pid, current->comm);
		return 0;
	}

    char buf[256] = {0};
	char* path = dentry_path_raw(args->dentry, buf, sizeof buf);
	pr_info("vfs_create[dentry = %s] -> %lu in proc[%d] %s\n", path, retval, current->pid, current->comm);
	return 0;
}

static struct kretprobe vfs_ret_probe = {
	.entry_handler	= on_vfs_create_ent,
	.handler		= on_vfs_create_ret,
	.data_size		= sizeof(struct vfs_create_args),
	.maxactive		= 100,
	.kp = {
		.symbol_name = "vfs_create",
	},
};

int __init init_module()
{
	int ret = register_kretprobe(&vfs_ret_probe);
	if (ret < 0) {
		pr_err("register_kretprobe failed, returned %d\n", ret);
		return -1;
	}
	pr_info("register_kretprobe ok, kretprobe at %p\n", vfs_ret_probe.kp.addr);
	return 0;
}

void __exit cleanup_module()
{
	unregister_kretprobe(&vfs_ret_probe);
}

MODULE_LICENSE("GPL");
