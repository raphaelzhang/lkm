# Linux Kernel Module demo

This is the Linux kernel module demo repo.

This repo contains demos for 'hello, world' projest, struct file\_operations and an [article](lkm_intro.md) for LKM intro. The codes are compiled and tested under kernel 3.16 and 4.4, but not for the old 2.6.X series.

Projects:

* hello: demo for printing "hello world" and parameter passing via `insmod` and `/sys/module/hello/parameters`
* dgst: demo for providing a `/proc/dgst` node for userland programs using file open/write/read/ioctl/close functions to access kernel data
* dgst\_userland: userland programs to access `/proc/dgst` file by using open/write/read/ioctl/close
* kprobes: it contains samples for jprobes and kretprobes, including cap\_capable, security\_file\_open and vfs\_create
* livepatch: it contains samples for various kernel function livepatches
* sysrq: it contains a sample to register a sysrq key to invoke a program

Hope you enjoy reading as much as I'm writing.

Codes are licensed under Dual BSD/GPL.

Happy Hacking ;)
